import { Outlet } from "react-router-dom";
import Sidebar from "../components/Sidebar";
import Header from "../components/Header";

const Root = () => {
  return (
    <div className="w-full min-h-screen bg-blue-600 flex flex-row">
      <Header titleHead="OFFICE || GLOTEK" />
      <Sidebar />
      <section className="flex-auto bg-blue-50 pt-[110px]">
        <div className="relative ml-60">
          <Outlet />
        </div>
      </section>
    </div>
  );
};

export default Root;
