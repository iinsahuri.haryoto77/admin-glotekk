import Root from "./root";
import Login from "../pages/Login";

import Home from "../pages/Home";
//
// import User from "../pages/User";
// import TableUser from "../pages/User/Table";
// import FormAddUser from "../pages/User/FormAdd";
// import FormEditUser from "../pages/User/FormEdit";
// import FormPassword from "../pages/User/FormPassword";

import Polda from "../pages/Polda";
import TablePolda from "../pages/Polda/Table";
import FormAddPolda from "../pages/Polda/FormAdd";
import FormEditPolda from "../pages/Polda/FormEdit";

import Samsat from "../pages/Samsat";
import TableSamsat from "../pages/Samsat/Table";
import FormAddSamsat from "../pages/Samsat/FormAdd";
import FormEditSamsat from "../pages/Samsat/FormEdit";

import BbmKendaraan from "../pages/BbmKendaraan";
import TableBbmKendaraan from "../pages/BbmKendaraan/Table";
import FormAddBbmKendaraan from "../pages/BbmKendaraan/FormAdd";
import FormEditBbmKendaraan from "../pages/BbmKendaraan/FormEdit";

import JnsKendaraan from "../pages/JnsKendaraan";
import TableJnsKendaraan from "../pages/JnsKendaraan/Table";
import FormAddJenisKendaraan from "../pages/JnsKendaraan/FormAdd";
import FormEditJenisKendaraan from "../pages/JnsKendaraan/FormEdit";

import ModelKendaraan from "../pages/ModelKendaraan";
import TableModelKendaraan from "../pages/ModelKendaraan/Table";
import FormAddModelK from "../pages/ModelKendaraan/FormAdd";
import FormEditNamaModelK from "../pages/ModelKendaraan/FormEdit";

import WarnaTNKB from "../pages/WarnaTNKB";
import TableWarnaTNKB from "../pages/WarnaTNKB/Table";
import FormAddWarnaTNKB from "../pages/WarnaTNKB/FormAdd";
import FormEditWarnaTNKB from "../pages/WarnaTNKB/FormEdit";

// import Vendor from "../pages/Vendor";
// import TableVendor from "../pages/Vendor/Table";
// import FormAddVendor from "../pages/Vendor/FormAdd";
// import FormEditVendor from "../pages/Vendor/FormEdit";

// import DaftarRenbut from "../pages/Renbut";
// import TableDaftarRenbut from "../pages/Renbut/Table";
// import FormAddDaftarRenbut from "../pages/Renbut/FormAdd";

import Order from "../pages/Order";
import TableOrder from "../pages/Order/Table";
import DetailOrder from "../pages/Order/Detail";

// import Kaworkshop from "../pages/Kaworkshop";
// import TableKaworkshop from "../pages/Kaworkshop/Table";
// import FormAddKaworkshop from "../pages/Kaworkshop/FormAdd";
// import FormEditKaworkshop from "../pages/Kaworkshop/FormEdit";
// import FormPasswordKaworkshop from "../pages/Kaworkshop/FormPassword";

import ErrorPage from "../pages/ErrorPage";

export const Paths = [
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "",
        element: <Home />,
      },
      // {
      //   path: "user",
      //   element: <User />,
      //   breadcrumb: "User",
      //   children: [
      //     {
      //       index: true,
      //       element: <TableUser />,
      //       breadcrumb: "User",
      //     },
      //     {
      //       path: "add",
      //       element: <FormAddUser />,
      //       breadcrumb: "Tambah User",
      //     },
      //     {
      //       path: ":id",
      //       element: <FormEditUser />,
      //       breadcrumb: "Edit User",
      //     },
      //     {
      //       path: "password/:id",
      //       element: <FormPassword />,
      //       breadcrumb: "Edit Password",
      //     },
      //   ],
      // },
      {
        path: "polda",
        element: <Polda />,
        breadcrumb: "Polda",
        children: [
          {
            index: true,
            element: <TablePolda />,
            breadcrumb: "Polda",
          },
          {
            path: "add",
            element: <FormAddPolda />,
            breadcrumb: "Tambah Polda Baru",
          },
          {
            path: ":id",
            element: <FormEditPolda />,
            breadcrumb: "Edit Polda",
          },
        ],
      },
      {
        path: "samsat",
        element: <Samsat />,
        breadcrumb: "Samsat",
        children: [
          {
            index: true,
            element: <TableSamsat />,
            breadcrumb: "Samsat",
          },
          {
            path: "add",
            element: <FormAddSamsat />,
            breadcrumb: "Tambah Samsat",
          },
          {
            path: ":id",
            element: <FormEditSamsat />,
            breadcrumb: "Edit Samsat",
          },
        ],
      },
      {
        path: "bbm-kendaraan",
        element: <BbmKendaraan />,
        breadcrumb: "BBM Kendaraan",
        children: [
          {
            index: true,
            element: <TableBbmKendaraan />,
            breadcrumb: "BBM Kendaraan",
          },
          {
            path: "add",
            element: <FormAddBbmKendaraan />,
            breadcrumb: "Tambah BBM Kendaraan",
          },
          {
            path: ":id",
            element: <FormEditBbmKendaraan />,
            breadcrumb: "Edit BBM Kendaraan",
          },
        ],
      },
      {
        path: "jenis-kendaraan",
        element: <JnsKendaraan />,
        breadcrumb: "Jenis Kendaraan",
        children: [
          {
            index: true,
            element: <TableJnsKendaraan />,
            breadcrumb: "Jenis Kendaraan",
          },
          {
            path: "add",
            element: <FormAddJenisKendaraan />,
            breadcrumb: "Tambah Jenis Kendaraan",
          },
          {
            path: ":id",
            element: <FormEditJenisKendaraan />,
            breadcrumb: "Edit Jenis Kendaraan",
          },
        ],
      },
      {
        path: "model-kendaraan",
        element: <ModelKendaraan />,
        breadcrumb: "Model Kendaraan",
        children: [
          {
            index: true,
            element: <TableModelKendaraan />,
            breadcrumb: "Model Kendaraan",
          },
          {
            path: "add",
            element: <FormAddModelK />,
            breadcrumb: "Tambah Model Kendaraan",
          },
          {
            path: ":id",
            element: <FormEditNamaModelK />,
            breadcrumb: "Edit Model Kendaraan",
          },
        ],
      },
      {
        path: "warna-tnkb",
        element: <WarnaTNKB />,
        breadcrumb: "Warna TNKB",
        children: [
          {
            index: true,
            element: <TableWarnaTNKB />,
            breadcrumb: "Warna TNKB",
          },
          {
            path: "add",
            element: <FormAddWarnaTNKB />,
            breadcrumb: "Tambah Warna TNKB",
          },
          {
            path: ":id",
            element: <FormEditWarnaTNKB />,
            breadcrumb: "Edit Warna TNKB",
          },
        ],
      },
      // {
      //   path: "vendor",
      //   element: <Vendor />,
      //   breadcrumb: "Vendor",
      //   children: [
      //     {
      //       index: true,
      //       element: <TableVendor />,
      //       breadcrumb: "Vendor",
      //     },
      //     {
      //       path: "add",
      //       element: <FormAddVendor />,
      //       breadcrumb: "Tambah Vendor",
      //     },
      //     {
      //       path: ":id",
      //       element: <FormEditVendor />,
      //       breadcrumb: "Edit Vendor",
      //     },
      //   ],
      // },
      // {
      //   path: "daftar-renbut",
      //   element: <DaftarRenbut />,
      //   breadcrumb: "Daftar Renbut",
      //   children: [
      //     {
      //       index: true,
      //       element: <TableDaftarRenbut />,
      //       breadcrumb: "Daftar Renbut",
      //     },
      //     {
      //       path: "add",
      //       element: <FormAddDaftarRenbut />,
      //       breadcrumb: "Buat Renbut",
      //     },
      //   ],
      // },
      {
        path: "order",
        element: <Order />,
        breadcrumb: "Order",
        children: [
          {
            index: true,
            element: <TableOrder />,
            breadcrumb: "Order",
          },
          {
            path: ":id",
            element: <DetailOrder />,
            breadcrumb: "Detail Order",
          },
        ],
      },
      // {
      //   path: "kaworkshop",
      //   element: <Kaworkshop />,
      //   breadcrumb: "Data Kaworkshop",
      //   children: [
      //     {
      //       index: true,
      //       element: <TableKaworkshop />,
      //       breadcrumb: "Data Kaworkshop",
      //     },
      //     {
      //       path: "add",
      //       element: <FormAddKaworkshop />,
      //       breadcrumb: "Add Kaworkshop",
      //     },
      //     {
      //       path: ":id",
      //       element: <FormEditKaworkshop />,
      //       breadcrumb: "Edit Kaworkshop",
      //     },
      //     {
      //       path: "password/:id",
      //       element: <FormPasswordKaworkshop />,
      //       breadcrumb: "Detail Kaworkshop",
      //     },
      //   ],
      // },
    ],
  },
  {
    path: "/login",
    element: <Login />,
  },
];
