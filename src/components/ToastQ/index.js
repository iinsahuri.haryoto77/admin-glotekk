import React, { Fragment } from "react";
import { Transition } from "@headlessui/react";

export class ToastQ extends React.Component {
  constructor(props) {
    super(props);
    console.log(props.msg);
    this.state = { message: props.msg, isShowing: props.isShow };
  }

  closeToast = () => {
    this.setState({
      isShowing: !this.state.isShowing,
    });
  };

  render() {
    return (
      <div className="absolute top-0 start-1/2 -translate-x-1/2 z-40">
        <Transition
          as={Fragment}
          show={this.state.isShowing}
          enter="transition duration-300 ease-in"
          enterFrom="-translate-y-96"
          enterTo="-translate-y-0"
          leave="duration-500 transition ease-in-out"
          leaveFrom="-translate-y-0"
          leaveTo="-translate-y-96"
        >
          <div
            className="max-w-xs bg-gray-800 text-sm text-white rounded-xl shadow-lg dark:bg-gray-900"
            role="alert"
          >
            <div className="flex p-4">
              {this.state.message}
              <div className="ms-auto">
                <button
                  type="button"
                  onClick={() => this.closeToast()}
                  className="inline-flex flex-shrink-0 justify-center items-center h-5 w-5 rounded-lg text-white hover:text-white opacity-50 hover:opacity-100 focus:outline-none focus:opacity-100"
                >
                  <span className="sr-only">Close</span>
                  <svg
                    className="flex-shrink-0 w-4 h-4"
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  >
                    <path d="M18 6 6 18" />
                    <path d="m6 6 12 12" />
                  </svg>
                </button>
              </div>
            </div>
          </div>
        </Transition>
      </div>
    );
  }
}

export default ToastQ;
