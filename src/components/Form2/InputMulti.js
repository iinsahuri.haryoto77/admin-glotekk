import React from "react";

const InputMulti = ({ label, name, set, val, placeholder }) => {
  return (
    <>
      <label
        htmlFor={name}
        className="block font-sans font-bold text-sm leading-6 text-gray-700"
      >
        {label}
      </label>
      <div className="mt-1">
        <input
          id={name}
          name={name}
          type="text"
          autoComplete="off"
          value={val}
          onChange={set}
          className="block w-full rounded-md border-0 py-2 text-gray-900 ring-1 ring-inset ring-gray-300 bg-white focus:bg-yellow-50 placeholder:text-gray-400 focus:ring-indigo-600 sm:text-sm sm:leading-6 font-sans"
          placeholder={placeholder}
        />
      </div>
    </>
  );
};

export default InputMulti;
