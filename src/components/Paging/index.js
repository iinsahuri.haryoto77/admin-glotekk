import React from "react";
import ReactPaginate from "react-paginate";

const Paging = ({
  totalRows,
  page,
  totalPages,
  limit,
  setLimit,
  changePage,
}) => {
  return (
    <div className="mx-2 py-4">
      <div className="flex flex-row items-center justify-between px-2">
        <div>
          <span className="text-sm font-sans">
            Total Rows: {totalRows} Page: {totalRows ? page + 1 : 0} of{" "}
            {totalPages}
          </span>
        </div>
        <div>
          <select
            id="showData"
            className="bg-white border-[0.5px] border-gray-300 text-gray-900 text-sm font-sans rounded-lg focus:ring-blue-500 focus:border-blue-500 py-2"
            value={limit}
            onChange={(e) => setLimit(e.target.value)}
          >
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="30">30</option>
            <option value="40">40</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
        </div>
        <nav aria-label="Page navigation example">
          <ReactPaginate
            previousLabel={"Prev"}
            nextLabel={"Next"}
            pageCount={parseInt(totalPages)}
            forcePage={page}
            pageRangeDisplayed={5}
            onPageChange={changePage}
            renderOnZeroPageCount={null}
            containerClassName={
              "inline-flex items-center -space-x-px text-sm font-sans"
            }
            pageLinkClassName={
              "px-3 py-2 leading-tight text-gray-500 bg-white ring-1 ring-gray-800 ring-opacity-5 ring-inset hover:bg-gray-200 hover:text-gray-700"
            }
            previousLinkClassName={
              "block px-3 py-2 ml-0 leading-tight text-gray-500 bg-white ring-1 ring-gray-800 ring-opacity-5 ring-inset rounded-l-lg hover:bg-gray-200 hover:text-gray-700"
            }
            nextLinkClassName={
              "block px-3 py-2 leading-tight text-gray-500 bg-white ring-1 ring-gray-800 ring-opacity-5 ring-inset rounded-r-lg hover:bg-gray-200 hover:text-gray-700"
            }
            activeLinkClassName={
              "z-10 px-3 py-2 leading-tight bg-gray-600 text-gray-100 cursor-auto ring-1 ring-gray-500 ring-opacity-5 ring-inset font-bold hover:bg-gray-600 hover:text-gray-100"
            }
            disabledLinkClassName={
              "z-10 px-3 py-2 leading-tight text-gray-500 ring-1 ring-gray-800 ring-opacity-5 ring-inset cursor-auto bg-gray-100 hover:bg-gray-100"
            }
          />
        </nav>
      </div>
    </div>
  );
};

export default Paging;
