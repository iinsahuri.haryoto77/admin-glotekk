import React, { Fragment, useState, useEffect } from "react";
import jwtDecode from "jwt-decode";
import { Menu, Transition } from "@headlessui/react";
import { HiOutlineChevronDown, HiPlus } from "react-icons/hi";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

import InputText from "../Form2/InputText";

import { API_URL } from "../../config";

import useSWRMutation from "swr/mutation";
import { sendRequestPostWithToken } from "../../swr";
import Loading from "../../pages/Loading";

const ButtonChoose = ({ index, sparepartId, namaSparepart, satuan, close }) => {
  const [qty, setQty] = useState(0);
  const [hargaSatuan, setHargaSatuan] = useState(0);

  const navigate = useNavigate();

  const { trigger, isMutating } = useSWRMutation(
    `${API_URL}/po/tmp`,
    sendRequestPostWithToken
  );

  useEffect(() => {
    // get refresh token
    checkToken();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const checkToken = () => {
    const token = localStorage.getItem("token");
    if (token) {
      const decoded = jwtDecode(token);

      const currentDate = new Date();
      if (decoded.exp * 1000 < currentDate.getTime()) {
        navigate("/login");
      }
    } else {
      navigate("/login");
    }
  };

  const simpan = async (e) => {
    e.preventDefault();

    try {
      const result = await trigger({
        sparepartId: sparepartId,
        namaSparepart: namaSparepart,
        qty: qty,
        satuan: satuan,
        hargaSatuan: hargaSatuan,
      });

      Swal.fire({
        icon: "success",
        title: "Sukses!",
        text: result.message,
        confirmButtonText: "Oke",
        willClose: () => {
          close(false);
        },
      });
    } catch (e) {
      if (e.status > 400) {
        Swal.fire("Sorry!", e.info.message, "error");
      } else {
        Swal.fire("Sorry!", "Error, Silahkan ulangi kembali!", "error");
      }
    }
  };

  if (isMutating) return <Loading />;

  return (
    <>
      <Menu as="div" className="relative inline-block text-left">
        <div>
          <Menu.Button
            type="button"
            className="inline-flex w-full justify-center gap-x-1.5 rounded-md bg-white px-3 py-2 text-xs font-sans font-bold text-gray-700 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
            id={`menu-button${index}`}
            aria-expanded="true"
            aria-haspopup="true"
          >
            Pilih Sparepart
            <HiOutlineChevronDown size={14} />
          </Menu.Button>
        </div>

        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items
            className="absolute right-0 z-10 mt-2 w-52 origin-top-right rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none"
            role="menu"
            aria-orientation="vertical"
            aria-labelledby={`menu-button${index}`}
            tabIndex="-1"
          >
            <div className="py-1 px-2" role="none">
              <p className="font-sans text-xs text-center">{namaSparepart}</p>
              <form onSubmit={simpan}>
                <div className="grid grid-cols-1 gap-x-4 gap-y-1 sm:grid-cols-6">
                  <div className="sm:col-start-1 sm:col-span-12">
                    <InputText
                      label="Qty"
                      name="qty"
                      val={qty}
                      set={setQty}
                      placeholder="0"
                    />
                  </div>
                  <div className="sm:col-start-1 sm:col-span-12">
                    <InputText
                      label="Harga"
                      name="hargaSatuan"
                      val={hargaSatuan}
                      set={setHargaSatuan}
                      placeholder="0"
                    />
                  </div>
                </div>
                <div className="mt-3">
                  <button
                    type="submit"
                    className="text-white w-full bg-[#166534] hover:bg-[#166534]/90 active::bg-[#166534]/50  focus:outline-none focus:ring-[#166534]/50 font-sans font-bold rounded-lg text-sm px-3 py-2 text-center inline-flex items-center justify-center shadow-md"
                  >
                    <span className="mr-2">
                      <HiPlus size={18} />
                    </span>
                    <span>Tambah Sparepart</span>
                  </button>
                </div>
              </form>
            </div>
          </Menu.Items>
        </Transition>
      </Menu>
    </>
  );
};

export default ButtonChoose;
