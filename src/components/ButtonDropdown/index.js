import React, { Fragment } from "react";
import { Menu, Transition } from "@headlessui/react";
import { HiOutlineChevronDown } from "react-icons/hi";
import { Link } from "react-router-dom";

const ButtonDropdown = ({ index, listMenu }) => {
  return (
    <>
      <Menu as="div" className="relative inline-block text-left">
        <div>
          <Menu.Button
            type="button"
            className="inline-flex w-full justify-center gap-x-1.5 rounded-md bg-white px-3 py-2 text-xs font-sans font-bold text-gray-700 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
            id={`menu-button${index}`}
            aria-expanded="true"
            aria-haspopup="true"
          >
            Options
            <HiOutlineChevronDown size={14} />
          </Menu.Button>
        </div>

        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items
            className="absolute right-0 z-10 mt-2 w-40 origin-top-right rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none"
            role="menu"
            aria-orientation="vertical"
            aria-labelledby={`menu-button${index}`}
            tabIndex="-1"
          >
            <div className="py-1" role="none">
              {listMenu.length > 0 &&
                listMenu.map((menu, index2) => (
                  <Menu.Item key={`${index2}-menu-${index}`}>
                    {menu.type === "link" ? (
                      <Link
                        to={menu.url}
                        className="text-gray-500 block px-4 py-2 text-sm hover:bg-gray-200"
                        role="menuitem"
                        tabIndex="-1"
                        id="menu-item-0"
                      >
                        <div className="flex flex-row items-center">
                          {menu.icon}
                          <span className="ml-2">{menu.namaMenu}</span>
                        </div>
                      </Link>
                    ) : (
                      <button
                        onClick={menu.onClick}
                        className="text-gray-500 block w-full px-4 py-2 text-sm hover:bg-gray-200"
                        role="menuitem"
                        tabIndex="-1"
                        id="menu-item-0"
                      >
                        <div className="flex flex-row items-center">
                          {menu.icon}
                          <span className="ml-2">{menu.namaMenu}</span>
                        </div>
                      </button>
                    )}
                  </Menu.Item>
                ))}
            </div>
          </Menu.Items>
        </Transition>
      </Menu>
    </>
  );
};

export default ButtonDropdown;
