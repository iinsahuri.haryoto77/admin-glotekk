import React, { useState, useEffect } from "react";
import jwtDecode from "jwt-decode";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

import { API_URL } from "../../config/index";

import useSWR from "swr";
import useSWRMutation from "swr/mutation";
import {
  sendRequestGet,
  sendRequestGetById,
  sendRequestPostWithToken,
} from "../../swr";
import Loading from "../../pages/Loading";
import { HiArrowRight } from "react-icons/hi";
import InputText from "../Form2/InputText";
import Select from "../Form2/Select";

const ModalCheckout = ({ modalPo, subTotal }) => {
  const navigate = useNavigate();

  const [discount, setDiscount] = useState(0);
  const [vendorId, setVendorId] = useState(0);
  const [namaVendor, setNamaVendor] = useState("");
  const [ppn, setPpn] = useState(0);

  const { data, isLoading } = useSWR(
    `${API_URL}/vendor-select`,
    sendRequestGet
  );

  const { trigger, isMutating } = useSWRMutation(
    `${API_URL}/po/checkout`,
    sendRequestPostWithToken
  );

  const { trigger: triggerVendor, isMutating: isMutatingVendor } =
    useSWRMutation(`${API_URL}/vendor`, sendRequestGetById);

  // console.log(data);

  useEffect(() => {
    // get refresh token
    checkToken();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const checkToken = () => {
    const token = localStorage.getItem("token");
    if (token) {
      const decoded = jwtDecode(token);

      const currentDate = new Date();
      if (decoded.exp * 1000 < currentDate.getTime()) {
        navigate("/login");
      }
    } else {
      navigate("/login");
    }
  };

  const onchangeVendor = async (idV) => {
    setVendorId(idV);
    const vendorRes = await triggerVendor({ id: idV });

    if (vendorRes) {
      setNamaVendor(vendorRes.namaVendor);
    }
  };

  const simpan = async (e) => {
    e.preventDefault();

    try {
      const result = await trigger({
        vendorId: vendorId,
        namaVendor: namaVendor,
        subTotal: subTotal,
        discount: discount,
        ppn: ppn,
      });

      Swal.fire({
        icon: "success",
        title: "Sukses!",
        text: result.message,
        confirmButtonText: "Oke",
        willClose: () => {
          navigate(-1);
        },
      });
    } catch (e) {
      if (e.status > 400) {
        Swal.fire("Error!", e.info.message, "error");
      } else {
        Swal.fire("Error!", "Error, Silahkan ulangi kembali!", "error");
      }
    }
  };

  if (isMutating || isLoading || isMutatingVendor) return <Loading />;

  return (
    <div
      className="relative z-10"
      aria-labelledby="modal-title"
      role="dialog"
      aria-modal="true"
    >
      <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"></div>

      <div className="fixed inset-0 z-10 w-screen overflow-y-auto mt-28">
        <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-start sm:p-0">
          <div className="relative transform overflow-x-hidden overflow-y-auto rounded-lg bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg">
            <div className="bg-white px-3 pb-4 pt-5 sm:p-5 sm:pb-4">
              <div className="flex items-center justify-between border-b pb-3 rounded-t dark:border-gray-600">
                <h3
                  className="text-base font-sans font-semibold leading-6 text-gray-900"
                  id="modal-title"
                >
                  Proses PO Baru
                </h3>
                <button
                  type="button"
                  onClick={() => modalPo(false)}
                  className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center"
                  data-modal-hide="default-modal"
                >
                  <svg
                    className="w-3 h-3"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 14"
                  >
                    <path
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
                    />
                  </svg>
                  <span className="sr-only">Close modal</span>
                </button>
              </div>

              <div className="mt-2">
                <p className="font-sans text-sm">
                  Silahkan isi lengkap form dibawah ini untuk melanjutkan proses
                  PO.
                </p>

                <form onSubmit={simpan}>
                  <div className="mt-5 grid grid-cols-1 gap-x-4 gap-y-3 sm:grid-cols-6">
                    <div className="sm:col-start-1 sm:col-span-12">
                      <Select
                        label="Vendor"
                        name="vendorId"
                        set={onchangeVendor}
                        val={vendorId}
                        item={data.data}
                      />
                    </div>
                    <div className="sm:col-start-1 sm:col-span-12">
                      <InputText
                        label="Discount"
                        name="discount"
                        val={discount}
                        set={setDiscount}
                        placeholder="0"
                      />
                    </div>
                    <div className="sm:col-start-1 sm:col-span-12">
                      <InputText
                        label="PPN"
                        name="ppn"
                        val={ppn}
                        set={setPpn}
                        placeholder="0"
                      />
                    </div>
                  </div>
                  <div className="sm:col-start-1 sm:col-span-12">
                    <div className="bg-red-50 px-4 py-2 rounded-lg mt-3">
                      <p className="font-sans text-sm text-gray-400">
                        SUB TOTAL
                      </p>
                      <h3 className="font-sans text-2xl font-bold text-gray-600">
                        {new Intl.NumberFormat("id-ID", {
                          style: "currency",
                          currency: "IDR",
                        }).format(subTotal)}
                      </h3>
                    </div>
                  </div>
                  <div className="mt-10 flex items-center justify-end gap-x-2">
                    <button
                      type="submit"
                      disabled={isMutating}
                      className="text-white bg-[#1d4ed8] hover:bg-[#1d4ed8]/90 active::bg-[#1d4ed8]/50  focus:outline-none focus:ring-[#1d4ed8]/50 font-sans font-bold rounded-lg text-sm px-10 py-2 text-center inline-flex items-center shadow-md"
                    >
                      <span>Buat PO Sparepart</span>
                      <span className="ml-2">
                        <HiArrowRight size={18} />
                      </span>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalCheckout;
