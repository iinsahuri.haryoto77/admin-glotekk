import React, { useState, useEffect } from "react";
import jwtDecode from "jwt-decode";
import { useNavigate } from "react-router-dom";

import { API_URL } from "../../config/index";

import useSWR from "swr";
import { sendRequestGet } from "../../swr";
import Loading from "../../pages/Loading";
import Paging from "../../components/Paging";
import { HiOutlineRefresh, HiOutlineSearch } from "react-icons/hi";
import ButtonChoose from "../ButtonDropdown/ButtonChoose";

const ModalSparepart = ({ searchSp, close }) => {
  const navigate = useNavigate();

  const [page, setPage] = useState(0);
  const [itemPage, setItemPage] = useState(0);
  /* eslint-disable-next-line */
  const [limit, setLimit] = useState(10);
  const [search, setSearch] = useState("");
  const [searchQuery, setSearchQuery] = useState("");

  const { data, isLoading, mutate } = useSWR(
    `${API_URL}/sparepart?search=${searchQuery}&page=${page}&limit=${limit}`,
    sendRequestGet
  );

  // console.log(data);

  useEffect(() => {
    // get refresh token
    checkToken();

    if (searchSp !== "") {
      setSearch(searchSp);
      setSearchQuery(searchSp);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const checkToken = () => {
    const token = localStorage.getItem("token");
    if (token) {
      const decoded = jwtDecode(token);

      const currentDate = new Date();
      if (decoded.exp * 1000 < currentDate.getTime()) {
        navigate("/login");
      }
    } else {
      navigate("/login");
    }
  };

  const changePage = ({ selected }) => {
    setItemPage(selected);
    setPage(selected + 1);
  };

  const searchData = (e) => {
    e.preventDefault();
    mutate();
    setPage(0);
    setItemPage(0);
    setSearchQuery(search);
  };

  const resetPage = (e) => {
    e.preventDefault();
    mutate();
    setPage(0);
    setItemPage(0);
    setSearch("");
    setSearchQuery("");
    setLimit(10);
  };

  if (isLoading) return <Loading />;

  return (
    <div
      className="relative z-10"
      aria-labelledby="modal-title"
      role="dialog"
      aria-modal="true"
    >
      <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"></div>

      <div className="fixed inset-0 z-10 w-screen overflow-y-auto mt-28">
        <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-start sm:p-0">
          <div className="relative transform overflow-x-hidden overflow-y-auto rounded-lg bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-5xl">
            <div className="bg-white px-3 pb-4 pt-5 sm:p-5 sm:pb-4">
              <div className="flex items-center justify-between border-b pb-3 rounded-t dark:border-gray-600">
                <h3
                  className="text-base font-sans font-semibold leading-6 text-gray-900"
                  id="modal-title"
                >
                  Daftar Sparepart
                </h3>
                <button
                  type="button"
                  onClick={() => close(false)}
                  className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center"
                  data-modal-hide="default-modal"
                >
                  <svg
                    className="w-3 h-3"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 14"
                  >
                    <path
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
                    />
                  </svg>
                  <span className="sr-only">Close modal</span>
                </button>
              </div>

              <div className="mt-2">
                <p className="font-sans text-sm">
                  Silahkan klik pilih sparepart untuk memasukan ke daftar PO
                  Sparepart
                </p>

                <div className="bg-white rounded-lg min-h-96 mt-4 ">
                  <div className="relative">
                    <div className="flex flex-row px-4 py-2">
                      <form onSubmit={searchData}>
                        <div className="flex">
                          <div className="relative w-full">
                            <input
                              type="search"
                              id="search-dropdown"
                              className="block px-4 py-2 ps-8 w-full z-20 font-sans text-sm text-gray-900 bg-white rounded-lg border border-gray-300 focus:ring-gray-300 focus:bg-gray-50 focus:border-blue-500"
                              placeholder="Search..."
                              value={search}
                              onChange={(e) => setSearch(e.target.value)}
                            />
                            <div className="absolute inset-y-0 start-2 flex items-center">
                              <HiOutlineSearch size={24} color="#9ca3af" />
                            </div>
                          </div>
                        </div>
                      </form>

                      <div className="flex-1 text-end">
                        <button
                          type="button"
                          className="text-gray-500 bg-white hover:bg-gray-300 active:bg-gray-100 active:text-gray-300 focus:ring-4 focus:outline-none focus:ring-[#F9FAFB]/50 rounded-lg text-sm px-5 py-2 text-center font-sans font-bold inline-flex items-center mr-2 shadow-md"
                          onClick={resetPage}
                        >
                          <span className="mr-2">
                            <HiOutlineRefresh size={18} />
                          </span>
                          <span>Reset</span>
                        </button>
                      </div>
                    </div>
                    <table className="w-full font-sans text-xs text-left text-gray-500 border-b">
                      <thead className="border-b border-t text-gray-600 bg-white">
                        <tr>
                          <th scope="col" className="px-2 py-3">
                            NAMA SPAREPART
                          </th>
                          <th scope="col" className="px-2 py-3">
                            KETERANGAN
                          </th>
                          <th scope="col" className="px-2 py-3">
                            SATUAN
                          </th>
                          <th scope="col" className="px-2 py-3">
                            <span>AKSI</span>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {data ? (
                          <>
                            {data.data.map((val, index) => {
                              return (
                                <tr
                                  key={index}
                                  className="bg-white border-b hover:bg-gray-50"
                                >
                                  <td className="px-2 py-2 text-gray-900">
                                    <strong>{val.namaSparepart}</strong>
                                  </td>
                                  <td className="px-2 py-2 text-gray-900">
                                    {val.keterangan}
                                  </td>
                                  <td className="px-2 py-2 text-gray-900">
                                    {val.satuan}
                                  </td>
                                  <td className="px-2 py-2 text-center">
                                    <ButtonChoose
                                      index={index}
                                      sparepartId={val.idSparepart}
                                      namaSparepart={val.namaSparepart}
                                      satuan={val.satuan}
                                      close={close}
                                    />
                                  </td>
                                </tr>
                              );
                            })}
                          </>
                        ) : (
                          <tr className="bg-white border-b hover:bg-gray-50">
                            <td colSpan="7" className="px-2 py-2 text-center">
                              Data tidak ditemukan
                            </td>
                          </tr>
                        )}
                      </tbody>
                    </table>
                  </div>
                  {!isLoading && (
                    <Paging
                      totalRows={data.totalRows}
                      totalPages={data.totalPage}
                      page={itemPage}
                      limit={limit}
                      setLimit={setLimit}
                      changePage={changePage}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalSparepart;
