import React from "react";
import { NavLink } from "react-router-dom";

const NavLinkMenu = ({ icon, label, to }) => {
  return (
    <NavLink
      to={to}
      className={({ isActive }) =>
        isActive
          ? "relative flex flex-row items-center py-2 focus:outline-none hover:bg-slate-200 text-blue-600"
          : "relative flex flex-row items-center py-2 focus:outline-none hover:bg-slate-200 text-gray-600"
      }
    >
      <span className="inline-flex justify-center items-center ml-4">
        {icon}
      </span>
      <span className="ml-2 text-sm font-sans font-bold tracking-wide truncate">
        {label}
      </span>
    </NavLink>
  );
};

export default NavLinkMenu;
