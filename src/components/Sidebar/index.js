import React from "react";
import {
  HiOutlineHome,
  HiOutlineLogout,
  HiOutlineUser,
  HiOutlineOfficeBuilding,
  HiOutlineLibrary,
  HiOutlineUserGroup,
  HiOutlineUsers,
  HiOutlineTruck,
} from "react-icons/hi";
import { LuFuel } from "react-icons/lu";
import { IoLogoModelS } from "react-icons/io";
import { MdFormatColorFill } from "react-icons/md";

import NavLinkMenu from "./NavLinkMenu";

const Sidebar = () => {
  return (
    <div className="min-h-screen flex flex-col flex-auto flex-shrink-0 antialiased bg-gray-50 text-gray-800 fixed z-10">
      <div className="fixed flex flex-col top-0 left-0 w-60 bg-white h-full border-r pt-[95px]">
        <div className="overflow-y-auto overflow-x-hidden flex-grow">
          <ul className="flex flex-col py-2 space-y-1">
            <li className="px-5">
              <div className="flex flex-row items-center h-8">
                <div className="text-sm tracking-wide text-gray-500">
                  Navigation
                </div>
              </div>
            </li>
            <li>
              <NavLinkMenu
                icon={<HiOutlineHome size={21} />}
                label="Dashboard"
                to="/"
              />
            </li>
            <li className="px-5">
              <div className="flex flex-row items-center h-8">
                <div className="text-sm tracking-wide text-gray-500">
                  Data Master
                </div>
              </div>
            </li>
            <li>
              <NavLinkMenu
                icon={<HiOutlineOfficeBuilding size={21} />}
                label="Polda"
                to="/polda"
              />
            </li>
            <li>
              <NavLinkMenu
                icon={<HiOutlineLibrary size={21} />}
                label="Samsat"
                to="/samsat"
              />
            </li>
            <li>
              <NavLinkMenu
                icon={<LuFuel size={21} />}
                label="BBM Kendaraan"
                to="/bbm-kendaraan"
              />
            </li>
            <li>
              <NavLinkMenu
                icon={<HiOutlineTruck size={21} />}
                label="Jenis Kendaraan"
                to="/jenis-kendaraan"
              />
            </li>
            <li>
              <NavLinkMenu
                icon={<IoLogoModelS size={21} />}
                label="Model Kendaraan"
                to="/model-kendaraan"
              />
            </li>
            <li>
              <NavLinkMenu
                icon={<MdFormatColorFill size={21} />}
                label="Warna TNKB"
                to="/warna-tnkb"
              />
            </li>
            <li className="px-5">
              <div className="flex flex-row items-center h-8">
                <div className="text-sm tracking-wide text-gray-500">Order</div>
              </div>
            </li>
            <li>
              <NavLinkMenu
                icon={<HiOutlineTruck size={21} />}
                label="Order"
                to="/order"
              />
            </li>
            <li className="px-5">
              <div className="flex flex-row items-center h-8">
                <div className="text-sm tracking-wide text-gray-500">
                  Settings
                </div>
              </div>
            </li>
            <li>
              <NavLinkMenu
                icon={<HiOutlineUsers size={21} />}
                label="Kaworkshop"
                to="/kaworkshop"
              />
            </li>
            <li>
              <NavLinkMenu
                icon={<HiOutlineUserGroup size={21} />}
                label="Users"
                to="/user"
              />
            </li>
            <li>
              <NavLinkMenu
                icon={<HiOutlineUser size={21} />}
                label="Profile"
                to="/account"
              />
            </li>
            <li>
              <NavLinkMenu
                icon={<HiOutlineLogout size={21} />}
                label="Keluar"
                to="/logout"
              />
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
