import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import jwtDecode from "jwt-decode";
let timer;

const Home = () => {
  const navigate = useNavigate();
  const [namaUser, setNamaUser] = useState("");
  const [counterState, setCounter] = React.useState(0);

  useEffect(() => {
    checkToken();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const checkToken = () => {
    const token = localStorage.getItem("token");
    if (token) {
      const decoded = jwtDecode(token);

      const currentDate = new Date();
      if (decoded.exp * 1000 < currentDate.getTime()) {
        navigate("/login");
      } else {
        setNamaUser(decoded.namaUser);
      }
    } else {
      navigate("/login");
    }
  };

  useEffect(() => {
    clearInterval(timer);
    timer = setInterval(() => {
      if (counterState === 100) {
        clearInterval(timer);
        return;
      }
      setCounter((prev) => prev + 1);
      timer++;
    }, 10);

    return () => clearInterval(timer);
  }, [counterState]);

  return (
    <div>
      <div className="p-4 ">
        <p className="font-sans text-sm">
          Selamat Datang, <strong>{namaUser}</strong>!
        </p>
      </div>
    </div>
  );
};

export default Home;
