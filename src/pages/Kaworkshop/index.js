import { Outlet } from "react-router-dom";

const Petugas = () => {
  return <Outlet />;
};

export default Petugas;
