import React, { useState, useEffect } from "react";
import jwtDecode from "jwt-decode";
import Swal from "sweetalert2";

import { sendRequestGet, sendRequestPostWithToken } from "../../swr";
import { FaRegEye, FaRegEyeSlash } from "react-icons/fa";
import { HiOutlineArrowLeft, HiPlus } from "react-icons/hi";
import { useNavigate } from "react-router-dom";
import { API_URL } from "../../config/index";

import InputPassword from "../../components/Form/InputPassword";
import InputText from "../../components/Form2/InputText";
import Select from "../../components/Form2/Select";

import useSWRMutation from "swr/mutation";
import Loading from "../Loading";
import useSWR from "swr";
import InputTextKapital from "../../components/Form2/InputTextKapital";

const FormAdd = () => {
  const [namaLengkap, setNamaLengkap] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [samsatId, setSamsatId] = useState("");
  const [poldaId, setPoldaId] = useState("");
  const [isPassword, setIsPassword] = useState(false);

  const navigate = useNavigate();

  const { trigger, isMutating } = useSWRMutation(
    `${API_URL}/kaworkshop`,
    sendRequestPostWithToken
  );
  const { data: polda, isLoading } = useSWR(
    `${API_URL}/polda-select`,
    sendRequestGet
  );

  const { data: unit } = useSWR(
    `${API_URL}/samsat-select/${poldaId}`,
    sendRequestGet
  );

  useEffect(() => {
    // get refresh token
    checkToken();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const checkToken = () => {
    const token = localStorage.getItem("token");
    if (token) {
      const decoded = jwtDecode(token);

      const currentDate = new Date();
      if (decoded.exp * 1000 < currentDate.getTime()) {
        navigate("/login");
      }
    } else {
      navigate("/login");
    }
  };

  const simpan = async (e) => {
    e.preventDefault();

    try {
      const result = await trigger({
        namaUser: namaLengkap,
        username: username,
        password: password,
        samsatId: samsatId,
        poldaId: poldaId,
      });

      Swal.fire({
        icon: "success",
        title: "Sukses!",
        text: result.message,
        confirmButtonText: "Oke",
        willClose: () => {
          navigate(-1);
        },
      });
    } catch (e) {
      if (e.status > 400) {
        Swal.fire("Error!", e.info.message, "error");
      } else {
        Swal.fire("Error!", "Error, Silahkan ulangi kembali!", "error");
      }
    }
  };

  const handlePassword = () => {
    if (isPassword) {
      setIsPassword(false);
    } else {
      setIsPassword(true);
    }
  };

  if (isMutating || isLoading) return <Loading />;

  return (
    <div className="mx-3 px-4 py-4 rounded-lg drop-shadow-lg bg-white">
      <div className="border-b">
        <h2 className="font-sans font-bold text-lg text-gray-800">
          TAMBAH KAWORKSHOP BARU
        </h2>
        <p className="text-sm font-sans text-gray-500">
          Penambahan kaworkshop samsat.
        </p>
      </div>
      <form onSubmit={simpan}>
        <div className="mt-5 grid grid-cols-1 gap-x-4 gap-y-3 sm:grid-cols-1 max-w-[800px]">
          <div className="sm:col-start-1 sm:col-span-4">
            <InputTextKapital
              label="Nama Lengkap"
              name="namaLengkap"
              val={namaLengkap}
              set={setNamaLengkap}
              placeholder="Masukkan nama kaworkshop"
            />
          </div>
          <div className="flex gap-2 sm:col-start-1 sm:col-span-4">
            <div className="sm:col-start-1 sm:row-span-4 w-full">
              <InputText
                label="Username"
                name="username"
                val={username}
                set={setUsername}
                placeholder="Masukkan username"
              />
            </div>
            <div className="sm:col-start-1 sm:row-span-4 w-full relative">
              <InputPassword
                type={isPassword ? "text" : "password"}
                label="Password"
                name="password"
                set={setPassword}
                val={password}
              />
              <span
                className="absolute bottom-3 end-3"
                onClick={() => handlePassword()}
              >
                {isPassword ? <FaRegEye /> : <FaRegEyeSlash />}
              </span>
            </div>
          </div>
          <div className="sm:col-start-1 sm:col-span-4">
            <Select
              label="Polda"
              name="poldaId"
              set={setPoldaId}
              val={poldaId}
              item={polda.data}
            />
          </div>
          <div className="sm:col-start-1 sm:col-span-4">
            <Select
              label="Samsat"
              name="samsatId"
              set={setSamsatId}
              val={samsatId}
              item={unit ? unit.data : ""}
            />
          </div>
        </div>
        <div className="mt-10 flex items-center gap-x-2">
          <button
            type="button"
            className="text-gray-700 bg-[#fbbf24] hover:bg-[#fbbf24]/90 active::bg-[#fbbf24]/50  focus:outline-none focus:ring-[#b91c1c]/50 font-sans font-bold rounded-lg text-sm px-6 py-2 text-center inline-flex items-center shadow-md"
            onClick={() => navigate(-1)}
          >
            <span className="mr-2">
              <HiOutlineArrowLeft size={18} />
            </span>
            <span>Cancel</span>
          </button>
          <button
            type="submit"
            disabled={isMutating}
            className="text-white bg-[#1d4ed8] hover:bg-[#1d4ed8]/90 active::bg-[#1d4ed8]/50  focus:outline-none focus:ring-[#1d4ed8]/50 font-sans font-bold rounded-lg text-sm px-10 py-2 text-center inline-flex items-center shadow-md"
          >
            <span className="mr-2">
              <HiPlus size={18} />
            </span>
            <span>Simpan Kaworkshop</span>
          </button>
        </div>
      </form>
    </div>
  );
};

export default FormAdd;
