/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import jwtDecode from "jwt-decode";
import Swal from "sweetalert2";

import { API_URL } from "../../config/index";
import { useNavigate, useParams } from "react-router-dom";
import { HiArrowNarrowLeft, HiPencilAlt } from "react-icons/hi";

import InputText from "../../components/Form2/InputText";

import useSWR from "swr";
import useSWRMutation from "swr/mutation";
import { sendRequestGet, sendRequestPut } from "../../swr";
import Loading from "../Loading";
import ErrorAccess from "../ErrorPage/ErrorAccess";
import Select from "../../components/Form2/Select";
import InputTextKapital from "../../components/Form2/InputTextKapital";

const FormEdit = () => {
  const { id } = useParams();

  const [namaLengkap, setNamaLengkap] = useState("");
  const [userId, setUserId] = useState("");
  const [username, setUsername] = useState("");
  const [poldaId, setPoldaId] = useState("");
  const [samsatId, setSamsatId] = useState("");

  const navigate = useNavigate();

  const { data, error, isLoading } = useSWR(
    `${API_URL}/kaworkshop/${id}`,
    sendRequestGet
  );

  const { data: dataPolda, isLoading: isLoadingPolda } = useSWR(
    `${API_URL}/polda-select`,
    sendRequestGet
  );

  const { data: unit, error: errUnit } = useSWR(
    `${API_URL}/samsat-select/${poldaId}`,
    sendRequestGet
  );

  const { trigger, isMutating } = useSWRMutation(
    `${API_URL}/kaworkshop/${id}`,
    sendRequestPut
  );

  useEffect(() => {
    // get refresh token
    checkToken();

    if (!isLoading && data) {
      setNamaLengkap(data.namaLengkap);
      setUsername(data.username);
      setPoldaId(data.poldaId);
      setUserId(data.userId);
      setSamsatId(data.samsatId);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, isLoading]);

  const checkToken = () => {
    const token = localStorage.getItem("token");
    if (token) {
      const decoded = jwtDecode(token);

      const currentDate = new Date();
      if (decoded.exp * 1000 < currentDate.getTime()) {
        navigate("/login");
      }
    } else {
      navigate("/login");
    }
  };

  const simpan = async (e) => {
    e.preventDefault();

    try {
      const result = await trigger({
        userId: userId,
        namaLengkap: namaLengkap,
        username: username,
        poldaId: poldaId,
        samsatId: samsatId,
      });

      Swal.fire({
        icon: "success",
        title: "Sukses!",
        text: result.message,
        confirmButtonText: "Oke",
        willClose: () => {
          navigate(-1);
        },
      });
    } catch (e) {
      if (e.status > 400) {
        Swal.fire("Error!", e.info.message, "error");
      } else {
        Swal.fire("Error!", "Error, Silahkan ulangi kembali!", "error");
      }
    }
  };

  if (isLoading || isLoadingPolda) return <Loading />;

  if (error || errUnit) return <ErrorAccess />;

  return (
    <div className="mx-3 px-4 py-4 rounded-lg drop-shadow-lg bg-white">
      <div className="border-b">
        <h2 className="font-sans font-bold text-lg text-gray-800">
          EDIT KAWORKSHOP
        </h2>
        <p className="text-sm font-sans text-gray-500">
          Perubahan data kaworkshop.
        </p>
      </div>
      <form onSubmit={simpan}>
        <div className="mt-5 grid grid-cols-1 gap-x-4 gap-y-3 sm:grid-cols-6">
          <div className="sm:col-start-1 sm:col-span-4">
            <InputTextKapital
              label="Nama Lengkap"
              name="namaLengkap"
              val={namaLengkap}
              set={setNamaLengkap}
              placeholder="Masukkan nama lengkap"
            />
          </div>
          <div className="sm:col-start-1 sm:col-span-2">
            <InputText
              label="Username"
              name="username"
              val={username}
              set={setUsername}
              placeholder="Masukkan nama username"
            />
          </div>
          <div className="sm:col-start-1 sm:col-span-4">
            <Select
              label="Polda"
              name="poldaId"
              set={setPoldaId}
              val={poldaId}
              item={dataPolda.data}
            />
          </div>
          <div className="sm:col-start-1 sm:col-span-4">
            <Select
              label="Unit"
              name="unit"
              set={setSamsatId}
              val={samsatId}
              item={unit ? unit.data : ""}
            />
          </div>
        </div>
        <div className="mt-10 flex items-center gap-x-2">
          <button
            type="button"
            className="text-gray-700 bg-[#fbbf24] hover:bg-[#fbbf24]/90 active::bg-[#fbbf24]/50  focus:outline-none focus:ring-[#b91c1c]/50 font-sans font-bold rounded-lg text-sm px-5 py-2 text-center inline-flex items-center shadow-md"
            onClick={() => navigate(-1)}
          >
            <span className="mr-2">
              <HiArrowNarrowLeft />
            </span>
            <span>Cancel</span>
          </button>
          <button
            type="submit"
            disabled={isMutating}
            className="text-white bg-[#3b5998] hover:bg-[#3b5998]/90 active::bg-[#3b5998]/50  focus:outline-none focus:ring-[#3b5998]/50 font-sans font-bold rounded-lg text-sm px-5 py-2 text-center inline-flex items-center shadow-md"
          >
            <span className="mr-2">
              <HiPencilAlt />
            </span>
            <span>Update Petugas</span>
          </button>
        </div>
      </form>
    </div>
  );
};

export default FormEdit;
