import React, { useState, useEffect } from "react";
import jwtDecode from "jwt-decode";
import { Link, useNavigate } from "react-router-dom";

import { API_URL } from "../../config/index";
import {
  HiOutlineRefresh,
  HiOutlinePlusSm,
  HiOutlineSearch,
} from "react-icons/hi";

import Select from "../../components/Form2/Select";
import EmptyData from "../ErrorPage/EmptyData";

import useSWR from "swr";
import useSWRMutation from "swr/mutation";
import { sendRequestGet, sendRequestPostWithToken } from "../../swr";
import Loading from "../Loading";

const Table = () => {
  const navigate = useNavigate();
  const [showData, setShowData] = useState(false);
  const [tahun, setTahun] = useState("");

  const [dataThn, setDataThn] = useState([]);
  /* eslint-disable-next-line */

  const { data, isLoading, mutate, isMutating } = useSWR(
    `${API_URL}/laporan/data-laporan-renbut`,
    sendRequestGet
  );

  const { trigger, isMutating: isMutating2 } = useSWRMutation(
    `${API_URL}/laporan/create-laporan-renbut`,
    sendRequestPostWithToken
  );

  // console.log(data);

  useEffect(() => {
    // get refresh token
    checkToken();
    loadTahun();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const checkToken = () => {
    const token = localStorage.getItem("token");
    if (token) {
      const decoded = jwtDecode(token);

      const currentDate = new Date();
      if (decoded.exp * 1000 < currentDate.getTime()) {
        navigate("/login");
      }
    } else {
      navigate("/login");
    }
  };

  const loadTahun = () => {
    const dataTh = [];
    for (let index = new Date().getFullYear(); index > 2021; index--) {
      dataTh.push({
        name: `${index}`,
        value: index,
      });
    }

    setDataThn(dataTh);
  };

  const searchData = async (e) => {
    e.preventDefault();

    const dataRes = await trigger({
      tahun: tahun,
    });

    if (dataRes.message === "Berhasil menampilkan data renbut") {
      setShowData(true);
      mutate();
    } else {
      setShowData(false);
    }
  };

  const resetPage = (e) => {
    e.preventDefault();
    mutate();
  };

  if (isLoading || isMutating || isMutating2) return <Loading />;

  // if (error)
  //   return (

  //   );

  return (
    <>
      <div className="drop-shadow-lg bg-white mx-3 px-4 py-4 mb-10 rounded-lg min-h-44">
        <div className="border-b">
          <h2 className="font-sans font-bold text-lg text-gray-800">
            Filter By
          </h2>
          <p className="text-sm font-sans text-gray-500">
            Filter terlebih dahulu untuk mencari data.
          </p>
        </div>
        <form onSubmit={searchData}>
          <div className="mt-2 grid grid-cols-1 gap-x-4 gap-y-3 sm:grid-cols-12">
            <div className="sm:col-span-2">
              <Select
                label="Tahun"
                name="tahun"
                val={tahun}
                set={setTahun}
                item={dataThn}
              />
            </div>
            <div className="sm:col-span-3 mt-7">
              <button
                type="submit"
                className="text-white bg-[#16a34a] hover:bg-[#16a34a]/90 active::bg-[#16a34a]/50  focus:outline-none focus:ring-[#16a34a]/50 font-sans font-bold rounded-lg text-sm px-5 py-2 text-center inline-flex items-center shadow-md"
                onClick={searchData}
              >
                <span className="mr-2">
                  <HiOutlineSearch />
                </span>
                <span>Filter Data</span>
              </button>
            </div>
          </div>
        </form>
      </div>
      {showData ? (
        <>
          <div className="px-3 mb-3">
            <div className="flex flex-row items-center">
              <div className="flex-1 mr-3 font-sans font-bold text-lg">
                Daftar Renbut
              </div>
              <div className="flex-1 text-end">
                <button
                  type="submit"
                  className="text-gray-500 bg-white hover:bg-gray-300 active:bg-gray-100 active:text-gray-300 focus:ring-4 focus:outline-none focus:ring-[#F9FAFB]/50 rounded-lg text-sm px-5 py-2 text-center font-sans font-bold inline-flex items-center mr-2 shadow-md"
                  onClick={resetPage}
                >
                  <span className="mr-2">
                    <HiOutlineRefresh size={18} />
                  </span>
                  <span>Reset</span>
                </button>
                <Link
                  to="add"
                  className="text-white bg-[#2563eb] hover:bg-[#2563eb]/90 active::bg-[#2563eb]/50  focus:outline-none focus:ring-[#2563eb]/50 rounded-lg font-sans font-bold text-sm px-5 py-2 text-center inline-flex items-center shadow-md"
                >
                  <span className="mr-2">
                    <HiOutlinePlusSm size={18} />
                  </span>
                  <span>Tambah</span>
                </Link>
              </div>
            </div>
          </div>
          <div className="drop-shadow-lg bg-white mx-3 mt-3 mb-8 rounded-lg min-h-96">
            <div className="relative">
              <div className="px-4 py-2 border-b">
                <h2 className="font-sans font-bold text-lg text-blue-600">
                  Semua Daftar Renbut
                </h2>
              </div>
              <table className="w-full font-sans text-sm text-left text-gray-500 border-b">
                <thead className="border-b border-t text-gray-600 bg-white">
                  <tr>
                    <th rowSpan="2" scope="col" className="p-2">
                      #
                    </th>
                    <th rowSpan="2" scope="col" className="px-2 py-3">
                      NAMA
                    </th>
                    <th colSpan="2" className="text-center border-b">
                      TNKB
                    </th>
                    <th colSpan="2" className="text-center border-b">
                      TCKB
                    </th>
                  </tr>
                  <tr>
                    <th className="text-center w-[90px]">R2</th>
                    <th className="text-center w-[90px]">R4</th>
                    <th className="text-center w-[90px]">R2</th>
                    <th className="text-center w-[90px]">R4</th>
                  </tr>
                </thead>
                <tbody>
                  {data ? (
                    <>
                      {data.data.map((val, index) => {
                        return (
                          <tr
                            key={index}
                            className="bg-white border-b hover:bg-gray-50"
                          >
                            <td className="p-2">{index + 1}</td>
                            <td className="px-2 py-2 text-gray-900">
                              <strong>{val.namaPolda}</strong>
                            </td>
                            <td className="sm:col-span-3 text-center">
                              {new Intl.NumberFormat("id-ID").format(999)}
                            </td>
                            <td className="sm:col-span-3 text-center">
                              {new Intl.NumberFormat("id-ID").format(999)}
                            </td>
                            <td className="sm:col-span-3 text-center">
                              {new Intl.NumberFormat("id-ID").format(999)}
                            </td>
                            <td className="sm:col-span-3 text-center">
                              {new Intl.NumberFormat("id-ID").format(999)}
                            </td>
                          </tr>
                        );
                      })}
                    </>
                  ) : (
                    <tr className="bg-white border-b hover:bg-gray-50">
                      <td colSpan="11" className="px-2 py-2 text-center">
                        Data tidak ditemukan
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </>
      ) : (
        <EmptyData
          msgTitle="Data tidak ditemukan!"
          msgDesc="Silahkan klik tombol dibawah ini untuk menambahakan data"
          button={
            <Link
              to="add"
              className="text-white bg-[#2563eb] hover:bg-[#2563eb]/90 active::bg-[#2563eb]/50  focus:outline-none focus:ring-[#2563eb]/50 rounded-lg font-sans font-bold text-sm px-5 py-2 text-center inline-flex items-center shadow-md"
            >
              <span className="mr-2">
                <HiOutlinePlusSm size={18} />
              </span>
              <span>Tambah Data Baru</span>
            </Link>
          }
        />
      )}
    </>
  );
};

export default Table;
