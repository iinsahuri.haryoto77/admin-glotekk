import React, { useState, useEffect } from "react";
import jwtDecode from "jwt-decode";
import Swal from "sweetalert2";

import { API_URL } from "../../config/index";
import { useNavigate } from "react-router-dom";
import { HiOutlineArrowLeft, HiPlus, HiOutlineSearch } from "react-icons/hi";

import Select from "../../components/Form2/Select";
import InputMulti from "../../components/Form2/InputMulti";

import useSWR from "swr";
import useSWRMutation from "swr/mutation";
import { sendRequestPostWithToken, sendRequestGet } from "../../swr";
import Loading from "../Loading";

const FormAdd = () => {
  const [poldaData, setPoldaData] = useState([]);

  const navigate = useNavigate();

  const [tahun, setTahun] = useState("");

  const [dataThn, setDataThn] = useState([]);
  // /* eslint-disable-next-line */

  const { trigger, isMutating } = useSWRMutation(
    `${API_URL}/unityan`,
    sendRequestPostWithToken
  );

  const { data, isLoading } = useSWR(`${API_URL}/polda-select`, sendRequestGet);

  useEffect(() => {
    // get refresh token
    checkToken();
    loadTahun();

    if (data) {
      data.data.forEach((rb) => {
        setPoldaData((poldaData) => [
          ...poldaData,
          {
            poldaId: rb.value,
            namaPolda: rb.name,
            totRenbutTnkbR2: 0,
            totRenbutTnkbR4: 0,
            totRenbutTckbR2: 0,
            totRenbutTckbR4: 0,
          },
        ]);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  const checkToken = () => {
    const token = localStorage.getItem("token");
    if (token) {
      const decoded = jwtDecode(token);

      const currentDate = new Date();
      if (decoded.exp * 1000 < currentDate.getTime()) {
        navigate("/login");
      }
    } else {
      navigate("/login");
    }
  };

  const setTotRenbutTnkbR2 = (idPolda, value) => {
    const polda = [...poldaData];
    const renbut = polda.find((a) => a.poldaId === idPolda);
    renbut.totRenbutTnkbR2 = value;
    setPoldaData(polda);
  };

  const setTotRenbutTnkbR4 = (idPolda, value) => {
    const polda = [...poldaData];
    const renbut = polda.find((a) => a.poldaId === idPolda);
    renbut.totRenbutTnkbR4 = value;
    setPoldaData(polda);
  };

  const setTotRenbutTckbR2 = (idPolda, value) => {
    const polda = [...poldaData];
    const renbut = polda.find((a) => a.poldaId === idPolda);
    renbut.totRenbutTckbR2 = value;
    setPoldaData(polda);
  };

  const setTotRenbutTckbR4 = (idPolda, value) => {
    const polda = [...poldaData];
    const renbut = polda.find((a) => a.poldaId === idPolda);
    renbut.totRenbutTckbR4 = value;
    setPoldaData(polda);
  };

  const simpan = async (e) => {
    e.preventDefault();

    try {
      const result = await trigger({
        poldaData: poldaData,
      });

      Swal.fire({
        icon: "success",
        title: "Sukses!",
        text: result.message,
        confirmButtonText: "Oke",
        willClose: () => {
          navigate(-1);
        },
      });
    } catch (e) {
      if (e.status > 400) {
        Swal.fire("Error!", e.info.message, "error");
      } else {
        Swal.fire("Error!", "Error, Silahkan ulangi kembali!", "error");
      }
    }
  };

  const loadTahun = () => {
    const dataTh = [];
    for (let index = new Date().getFullYear(); index > 2021; index--) {
      dataTh.push({
        name: `${index}`,
        value: index,
      });
    }

    setDataThn(dataTh);
  };

  const searchData = (e) => {
    e.preventDefault();
  };

  if (isMutating || isLoading) return <Loading />;

  return (
    <div className="mx-3 px-4 py-4 rounded-lg drop-shadow-lg bg-white">
      <div className="border-b">
        <h2 className="font-sans font-bold text-lg text-gray-800">
          TAMBAH RENBUT BARU
        </h2>
        <p className="text-sm font-sans text-gray-500">
          Penambahan renbut baru.
        </p>
      </div>
      <div className="border-b">
        <form onSubmit={searchData}>
          <div className="mt-2 grid grid-cols-1 gap-x-4 gap-y-3 sm:grid-cols-12 mb-5">
            <div className="sm:col-start-1 sm:col-span-3">
              <Select
                label="Tahun"
                name="thn"
                val={tahun}
                set={setTahun}
                item={dataThn}
              />
              <p className="text-sm font-sans text-gray-500">
                Pilih tahun untuk input data renbut dibawah!
              </p>
            </div>

            <div className="sm:col-span-3 mt-7">
              <button
                type="submit"
                className="text-white bg-[#16a34a] hover:bg-[#16a34a]/90 active::bg-[#16a34a]/50  focus:outline-none focus:ring-[#16a34a]/50 font-sans font-bold rounded-lg text-sm px-5 py-2 text-center inline-flex items-center shadow-md"
                onClick={searchData}
              >
                <span className="mr-2">
                  <HiOutlineSearch />
                </span>
                <span>Filter Data</span>
              </button>
            </div>
          </div>
        </form>
      </div>
      <form onSubmit={simpan}>
        <>
          {poldaData.map((val, index) => {
            return (
              <div key={index}>
                <div className="mt-5 grid grid-cols-1 gap-x-4 gap-y-3 sm:grid-cols-5">
                  <div className="sm:col-start-1 sm:col-span-1 block font-sans font-bold text-sm leading-6 text-gray-700 mt-8">
                    {index + 1}. {val.namaPolda}
                  </div>
                  <div className="sm:col-span-1">
                    <InputMulti
                      key={index}
                      label="TNKB Roda 2"
                      name="latitude"
                      val={val.totRenbutTnkbR2}
                      set={(e) =>
                        setTotRenbutTnkbR2(val.poldaId, e.target.value)
                      }
                      placeholder="0"
                    />
                    <p className="mt-1 text-xs font-sans leading-6 text-gray-300">
                      Ketik Total TNKB R2
                    </p>
                  </div>
                  <div className="sm:col-span-1">
                    <InputMulti
                      label="TNKB Roda 4"
                      name="longitude"
                      val={val.totRenbutTnkbR4}
                      set={(e) =>
                        setTotRenbutTnkbR4(val.poldaId, e.target.value)
                      }
                      placeholder="0"
                    />
                    <p className="mt-1 text-xs font-sans leading-6 text-gray-300">
                      Ketik Total TNKB R4
                    </p>
                  </div>
                  <div className="sm:col-span-1">
                    <InputMulti
                      label="TCKB Roda 2"
                      name="latitude"
                      val={val.totRenbutTckbR2}
                      set={(e) =>
                        setTotRenbutTckbR2(val.poldaId, e.target.value)
                      }
                      placeholder="0"
                    />
                    <p className="mt-1 text-xs font-sans leading-6 text-gray-300">
                      Ketik Total TCKB R2
                    </p>
                  </div>
                  <div className="sm:col-span-1">
                    <InputMulti
                      label="TCKB Roda 4"
                      name="longitude"
                      val={val.totRenbutTckbR4}
                      set={(e) =>
                        setTotRenbutTckbR4(val.poldaId, e.target.value)
                      }
                      placeholder="0"
                    />
                    <p className="mt-1 text-xs font-sans leading-6 text-gray-300">
                      Ketik Total TCKB R4
                    </p>
                  </div>
                </div>
              </div>
            );
          })}
        </>
        <div className="mt-10 flex items-center gap-x-2">
          <button
            type="button"
            className="text-gray-700 bg-[#fbbf24] hover:bg-[#fbbf24]/90 active::bg-[#fbbf24]/50  focus:outline-none focus:ring-[#b91c1c]/50 font-sans font-bold rounded-lg text-sm px-6 py-2 text-center inline-flex items-center shadow-md"
            onClick={() => navigate(-1)}
          >
            <span className="mr-2">
              <HiOutlineArrowLeft size={18} />
            </span>
            <span>Cancel</span>
          </button>
          <button
            type="submit"
            disabled={isMutating}
            className="text-white bg-[#1d4ed8] hover:bg-[#1d4ed8]/90 active::bg-[#1d4ed8]/50  focus:outline-none focus:ring-[#1d4ed8]/50 font-sans font-bold rounded-lg text-sm px-10 py-2 text-center inline-flex items-center shadow-md"
          >
            <span className="mr-2">
              <HiPlus size={18} />
            </span>
            <span>Simpan Renbut</span>
          </button>
        </div>
      </form>
    </div>
  );
};

export default FormAdd;
