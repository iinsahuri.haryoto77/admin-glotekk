import { Outlet } from "react-router-dom";

const Polda = () => {
  return <Outlet />;
};

export default Polda;
