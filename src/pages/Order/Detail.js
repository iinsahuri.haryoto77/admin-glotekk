import React, { useEffect } from "react";
import jwtDecode from "jwt-decode";

import { API_URL_OFFICE } from "../../config/index";
import { useNavigate, useParams } from "react-router-dom";
import { HiArrowNarrowLeft, HiChevronLeft } from "react-icons/hi";

import useSWR from "swr";
import { sendRequestGet } from "../../swr";
import Loading from "../Loading";
import ErrorAccess from "../ErrorPage/ErrorAccess";

const Detail = () => {
  const { id } = useParams();

  const navigate = useNavigate();

  const { data, error, isLoading } = useSWR(
    `${API_URL_OFFICE}/order/${id}`,
    sendRequestGet
  );

  const {
    data: detail,
    error: erDetail,
    isLoading: isLoadDetail,
  } = useSWR(`${API_URL_OFFICE}/order/${id}`, sendRequestGet);

  useEffect(() => {
    // get refresh token
    checkToken();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const checkToken = () => {
    const token = localStorage.getItem("token");
    if (token) {
      const decoded = jwtDecode(token);

      const currentDate = new Date();
      if (decoded.exp * 1000 < currentDate.getTime()) {
        navigate("/login");
      }
    } else {
      navigate("/login");
    }
  };

  if (isLoadDetail || isLoading) return <Loading />;
  if (erDetail || error) return <ErrorAccess />;

  return (
    <div className="mx-3 px-4 py-4 rounded-lg drop-shadow-lg bg-white mb-6">
      <div className="">
        <div className="flex items-center gap-2">
          <button className="btn hover:scale-110" onClick={() => navigate(-1)}>
            <HiChevronLeft />
          </button>
          <h2 className=" font-bold text-lg text-gray-800"> Detail Order</h2>
        </div>
      </div>
      <div className="flex gap-4 flex-col lg:flex-row">
        <div className="container mx-auto mt-[1.8rem]">
          <div className="border-b">
            <h2 className=" font-bold text-lg text-gray-800"> Data Order</h2>
          </div>
          <div className="flex flex-col gap-3 mt-6">
            <div className="flex flex-col">
              <h3 className=" font-bold text-sm text-gray-800">No. Order</h3>
              <h3 className=" text-sm text-gray-800">{data.nomorOrder}</h3>
            </div>
            <div className="flex flex-col">
              <h3 className=" font-bold text-sm text-gray-800">Nama</h3>
              <h3 className=" text-sm text-gray-800">{data.namaLengkap}</h3>
            </div>
            <div className="flex flex-col">
              <h3 className=" font-bold text-sm text-gray-800">Samsat</h3>
              <h3 className=" text-sm text-gray-800">{data.namaSamsat}</h3>
            </div>
            <div className="flex gap-5">
              <div className="flex flex-col">
                <h3 className=" font-bold text-sm text-gray-800">Polda</h3>
                <h3 className=" text-sm text-gray-800">{data.namaPolda}</h3>
              </div>
            </div>
          </div>
        </div>

        <div className="container mx-auto mt-[1.8rem]">
          <div className="border-b">
            <h2 className=" font-bold text-lg text-gray-800">Data Kendaraan</h2>
          </div>
          <div className="flex flex-col gap-3 mt-6">
            <div className="flex flex-col">
              <h3 className=" font-bold text-sm text-gray-800">
                Tanggal Order
              </h3>
              <h3 className=" text-sm text-gray-800">{data.tglOrder}</h3>
            </div>
            <div className="flex flex-col">
              <h3 className=" font-bold text-sm text-gray-800">Status</h3>
              <h3 className=" text-sm text-gray-800">
                {data.statusOrder === 1 ? (
                  <span className="px-2 py-0.5 ml-auto text-xs font-sans tracking-wide text-white bg-emerald-500 rounded-md">
                    SELESAI
                  </span>
                ) : (
                  ""
                )}
                {data.statusOrder === 2 ? (
                  <span className="px-2 py-0.5 ml-auto text-xs font-sans tracking-wide text-white bg-red-500 rounded-md">
                    SEDANG DIKERJAKAN
                  </span>
                ) : (
                  ""
                )}
                {data.statusOrder === 3 ? (
                  <span className="px-2 py-0.5 ml-auto text-xs font-sans tracking-wide text-white bg-red-500 rounded-md">
                    ORDER
                  </span>
                ) : (
                  ""
                )}
              </h3>
            </div>
            <div className="flex gap-5">
              <div className="flex flex-col">
                <h3 className=" font-bold text-sm text-gray-800">
                  Kategori Order
                </h3>
                <h3 className=" text-sm text-gray-800">
                  {data.kategoriOrder === "UMUM" ? (
                    <span className="px-2 py-0.5 ml-auto text-xs font-sans tracking-wide text-white bg-blue-500 rounded-md">
                      UMUM
                    </span>
                  ) : (
                    <span className="px-2 py-0.5 ml-auto text-xs font-sans tracking-wide text-white bg-orange-500 rounded-md">
                      KHUSUS
                    </span>
                  )}
                </h3>
              </div>
            </div>
            <div className="flex gap-5">
              <div className="flex flex-col">
                <h3 className=" font-bold text-sm text-gray-800">Keterangan</h3>
                <h3 className=" text-sm text-gray-800">{data.keterangan}</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container mx-auto mt-[1.8rem]">
        <div className="border-b">
          <h2 className=" font-bold text-lg text-gray-800">
            {" "}
            Detail Data Kendaraan
          </h2>
        </div>
        <div className="relative overflow-x-auto mb-4">
          <table className="w-full text-sm text-left text-gray-500">
            <thead className="text-sm text-gray-700 uppercase bg-gray-100">
              <tr>
                <th scope="col" className="px-6 py-3">
                  NO
                </th>
                <th scope="col" className="px-6 py-3">
                  NOMOR TNKB
                </th>
                <th scope="col" className="px-6 py-3">
                  BULAN/TAHUN
                </th>
                <th scope="col" className="px-6 py-3">
                  NAMA MODEL KENDARAAN
                </th>
                <th scope="col" className="px-6 py-3">
                  NAMA BBM
                </th>
                <th scope="col" className="px-6 py-3">
                  JENIS KENDARAAN
                </th>
                <th scope="col" className="px-6 py-3">
                  WARNA TNKB
                </th>
              </tr>
            </thead>
            <tbody>
              {detail.detailOrder.length > 0 &&
                detail.detailOrder.map((row, index) => (
                  <tr
                    key={`data-${index}`}
                    className="bg-white border-b center"
                  >
                    <th
                      scope="row"
                      className="px-6 py-3 text-sm text-gray-900 w-1"
                    >
                      {index + 1}
                    </th>
                    <td className="px-6 py-3 text-sm  capitalize text-black">
                      <strong>
                        {row.kodeWilPolda}&nbsp;
                        {row.noPol}&nbsp;
                        {row.kodeWilayah}
                      </strong>
                    </td>
                    <td className="px-6 py-3 text-sm  capitalize">
                      {row.blnThn}
                    </td>
                    <td className="px-6 py-3 text-sm  capitalize">
                      {row.namaModelKendaraan}
                    </td>
                    <td className="px-6 py-3 text-sm  capitalize">
                      {row.namaBbm}
                    </td>
                    <td className="px-6 py-3 text-sm  capitalize">
                      {row.jenisKendaraan}
                    </td>
                    <td className="px-6 py-3 text-sm  capitalize">
                      {row.warnaTnkb}
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
          <div className="flex flex-col md:flex-row mt-4">
            <div className="flex w-[80%] gap-3">
              <button
                type="button"
                className="text-gray-700 bg-[#fbbf24] hover:bg-[#fbbf24]/90 active::bg-[#fbbf24]/50  focus:outline-none focus:ring-[#b91c1c]/50  font-bold rounded-lg text-sm px-5 py-2 text-center inline-flex items-center shadow-md"
                onClick={() => navigate(-1)}
              >
                <span className="mr-2">
                  <HiArrowNarrowLeft size={20} />
                </span>
                <span>Kembali</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Detail;
