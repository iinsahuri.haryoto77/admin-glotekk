import React, { useState, useEffect } from "react";
import jwtDecode from "jwt-decode";
import Swal from "sweetalert2";

import { API_URL } from "../../config/index";
import { useNavigate, useParams } from "react-router-dom";
import { HiArrowNarrowLeft, HiPencilAlt } from "react-icons/hi";

import InputText from "../../components/Form2/InputText";
import Radio from "../../components/Form2/Radio";

import useSWR from "swr";
import useSWRMutation from "swr/mutation";
import { sendRequestGet, sendRequestPut } from "../../swr";
import Loading from "../Loading";
import ErrorAccess from "../ErrorPage/ErrorAccess";

const FormEdit = () => {
  const { id } = useParams();

  const [noPol, setNoPol] = useState("");
  const [jnsCetak, setJnsCetak] = useState("");
  const [roda, setRoda] = useState("");

  const itemRoda = [
    {
      name: "R4",
      value: "R4"
    },
    {
      name: "R2",
      value: "R2",
    },
  ];
  const itemJnsCetak = [
    {
      name: "TNKB",
      value: "TNKB"
    },
    {
      name: "TCKB",
      value: "TCKB",
    },
  ];

  const navigate = useNavigate();

  const { data, error, isLoading } = useSWR(
    `${API_URL}/produksi/${id}`,
    sendRequestGet
  );

  const { trigger, isMutating } = useSWRMutation(
    `${API_URL}/produksi/${id}`,
    sendRequestPut
  );

  useEffect(() => {
    // get refresh token
    checkToken();

    if (!isLoading && data) {
      setNoPol(data.noPol);
      setJnsCetak(data.jnsCetak);
      setRoda(data.roda);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, isLoading]);

  const checkToken = () => {
    const token = localStorage.getItem("token");
    if (token) {
      const decoded = jwtDecode(token);

      const currentDate = new Date();
      if (decoded.exp * 1000 < currentDate.getTime()) {
        navigate("/login");
      }
    } else {
      navigate("/login");
    }
  };

  const simpan = async (e) => {
    e.preventDefault();

    try {
      const result = await trigger({
        noPol: noPol,
        jnsCetak: jnsCetak,
        roda: roda,
      });

      Swal.fire({
        icon: "success",
        title: "Sukses!",
        text: result.message,
        confirmButtonText: "Oke",
        willClose: () => {
          navigate(-1);
        },
      });
    } catch (e) {
      if (e.status > 400) {
        Swal.fire("Error!", e.info.message, "error");
      } else {
        Swal.fire("Error!", "Error, Silahkan ulangi kembali!", "error");
      }
    }
  };

  if (isLoading) return <Loading />;

  if (error) return <ErrorAccess />;

  return (
    <div className="mx-3 px-4 py-4 rounded-lg drop-shadow-lg bg-white">
      <div className="border-b">
        <h2 className="font-sans font-bold text-lg text-gray-800">
          EDIT DAFTAR PRODUKSI
        </h2>
        <p className="text-sm font-sans text-gray-500">Perubahan data daftar Produksi.</p>
      </div>
      <form onSubmit={simpan}>
        <div className="mt-5 grid grid-cols-1 gap-x-4 gap-y-3 sm:grid-cols-6">
          <div className="sm:col-start-1 sm:col-span-2">
            <InputText
              label="Nomor Polisi"
              name="noPol"
              val={noPol}
              set={setNoPol}
              placeholder="Masukkan nomor polisi"
            />
            <p className="mt-1 text-xs font-sans leading-6 text-gray-400">Contoh : B 1234 ABC</p>
          </div>
          <div className="sm:col-start-1 sm:col-span-2">
            <label className="block font-sans font-bold text-sm leading-6 text-gray-700">
              Jenis Cetak
            </label>
            <div className="grid grid-cols-3 gap-3 mt-1">
              {itemJnsCetak &&
                itemJnsCetak.map((item, index) => (
                  <Radio
                    key={`kat-${index}`}
                    name={item.name}
                    value={item.value}
                    setValue={jnsCetak}
                    onClick={() => setJnsCetak(item.value)}
                  />
                ))}
            </div>
          </div>
          <div className="sm:col-start-1 sm:col-span-2">
            <label className="block font-sans font-bold text-sm leading-6 text-gray-700">
              Roda
            </label>
            <div className="grid grid-cols-3 gap-3 mt-1">
              {itemRoda &&
                itemRoda.map((item, index) => (
                  <Radio
                    key={`kat-${index}`}
                    name={item.name}
                    value={item.value}
                    setValue={roda}
                    onClick={() => setRoda(item.value)}
                  />
                ))}
            </div>
          </div>
        </div>
        <div className="mt-10 flex items-center gap-x-2">
          <button
            type="button"
            className="text-gray-700 bg-[#fbbf24] hover:bg-[#fbbf24]/90 active::bg-[#fbbf24]/50  focus:outline-none focus:ring-[#b91c1c]/50 font-sans font-bold rounded-lg text-sm px-5 py-2 text-center inline-flex items-center shadow-md"
            onClick={() => navigate(-1)}
          >
            <span className="mr-2">
              <HiArrowNarrowLeft />
            </span>
            <span>Cancel</span>
          </button>
          <button
            type="submit"
            disabled={isMutating}
            className="text-white bg-[#3b5998] hover:bg-[#3b5998]/90 active::bg-[#3b5998]/50  focus:outline-none focus:ring-[#3b5998]/50 font-sans font-bold rounded-lg text-sm px-5 py-2 text-center inline-flex items-center shadow-md"
          >
            <span className="mr-2">
              <HiPencilAlt />
            </span>
            <span>Update Daftar Produksi</span>
          </button>
        </div>
      </form>
    </div>
  );
};

export default FormEdit;
