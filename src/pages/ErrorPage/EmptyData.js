import imgNotFound from "../../assets/images/not-found.webp";

const EmptyData = ({ button, msgTitle, msgDesc }) => {
  return (
    <div className="flex items-center justify-center mx-10">
      <div className="flex flex-col items-center">
        <img src={imgNotFound} className="w-60" alt="illustration-error" />
        <div className="text-center">
          <p className="text-4xl text-black font-sans md:text-6xl lg:text-8xl">
            {msgTitle}
          </p>
          <p className="text-lg font-sans mb-6 text-gray-500">{msgDesc}</p>
          {button}
        </div>
      </div>
    </div>
  );
};

export default EmptyData;
