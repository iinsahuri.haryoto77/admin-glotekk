import { Outlet } from "react-router-dom";

const JnsKendaraan = () => {
  return <Outlet />;
};

export default JnsKendaraan;
