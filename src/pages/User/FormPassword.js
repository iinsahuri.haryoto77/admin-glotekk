import React, { useState, useEffect } from "react";
import jwtDecode from "jwt-decode";
import Swal from "sweetalert2";

import { API_URL } from "../../config/index";
import { useNavigate, useParams } from "react-router-dom";
import { HiSave, HiArrowNarrowLeft } from "react-icons/hi";
import { FaRegEye, FaRegEyeSlash } from "react-icons/fa";

import InputPassword from "../../components/Form/InputPassword";

import useSWRMutation from "swr/mutation";
import { sendRequestPut } from "../../swr";
import Loading from "../Loading";

const FormPassword = () => {
  const { id } = useParams();
  const [password, setPassword] = useState("");
  const [isPassword, setIsPassword] = useState(false);

  const navigate = useNavigate();

  const { trigger, isMutating } = useSWRMutation(
    `${API_URL}/user/update-pw/${id}`,
    sendRequestPut
  );

  useEffect(() => {
    // get refresh token
    checkToken();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const checkToken = () => {
    const token = localStorage.getItem("token");
    if (token) {
      const decoded = jwtDecode(token);

      const currentDate = new Date();
      if (decoded.exp * 1000 < currentDate.getTime()) {
        navigate("/login");
      }
    } else {
      navigate("/login");
    }
  };

  const simpan = async (e) => {
    e.preventDefault();

    try {
      const result = await trigger({
        password: password,
      });

      Swal.fire({
        icon: "success",
        title: "Sukses!",
        text: result.message,
        confirmButtonText: "Oke",
        willClose: () => {
          navigate(-1);
        },
      });
    } catch (e) {
      if (e.status > 400) {
        Swal.fire("Error!", e.info.message, "error");
      } else {
        Swal.fire("Error!", "Error, Silahkan ulangi kembali!", "error");
      }
    }
  };

  const handlePassword = () => {
    if (isPassword) {
      setIsPassword(false);
    } else {
      setIsPassword(true);
    }
  };

  if (isMutating) return <Loading />;

  return (
    <div className="mx-3 px-4 py-4 rounded-lg drop-shadow-lg bg-white">
      <div className="border-b">
        <h2 className="font-sans font-bold text-lg text-gray-800">
          Ganti Password
        </h2>
        <p className="text-sm font-sans text-gray-500">
          Perubahan password user, isi informasi data dibawah ini.
        </p>
      </div>
      <form onSubmit={simpan}>
        <div className="mt-5 grid grid-cols-1 gap-x-4 gap-y-3 sm:grid-cols-6">
          <div className="col-start-1 md:col-span-3 col-span-3 relative">
            <InputPassword
              type={isPassword ? "text" : "password"}
              label="Password"
              name="password"
              set={setPassword}
              val={password}
              placeholder="***"
            />
            <span
              className="absolute bottom-10 end-3"
              onClick={() => handlePassword()}
            >
              {isPassword ? <FaRegEye /> : <FaRegEyeSlash />}
            </span>
            <p className="mt-1 text-xs font-sans leading-6 text-gray-400">
              Password untuk login ke aplikasi.
            </p>
          </div>
        </div>
        <div className="mt-10 flex items-center gap-x-2">
          <button
            type="button"
            className="text-gray-700 bg-[#fbbf24] hover:bg-[#fbbf24]/90 active::bg-[#fbbf24]/50  focus:outline-none focus:ring-[#b91c1c]/50 font-sans font-bold rounded-lg text-sm px-5 py-2 text-center inline-flex items-center shadow-md"
            onClick={() => navigate(-1)}
          >
            <span className="mr-2">
              <HiArrowNarrowLeft />
            </span>
            <span>Cancel</span>
          </button>
          <button
            type="submit"
            disabled={isMutating}
            className="text-white bg-[#3b5998] hover:bg-[#3b5998]/90 active::bg-[#3b5998]/50  focus:outline-none focus:ring-[#3b5998]/50 font-sans font-bold rounded-lg text-sm px-5 py-2 text-center inline-flex items-center shadow-md"
          >
            <span className="mr-2">
              <HiSave />
            </span>
            <span>Ganti Password</span>
          </button>
        </div>
      </form>
    </div>
  );
};

export default FormPassword;
