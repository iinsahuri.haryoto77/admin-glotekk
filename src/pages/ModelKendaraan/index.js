import { Outlet } from "react-router-dom";

const ModelKendaraan = () => {
  return <Outlet />;
};

export default ModelKendaraan;
