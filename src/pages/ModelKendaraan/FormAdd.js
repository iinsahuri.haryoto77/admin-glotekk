import React, { useState, useEffect } from "react";
import jwtDecode from "jwt-decode";
import Swal from "sweetalert2";

import { API_URL_MASTER } from "../../config/index";
import { useNavigate } from "react-router-dom";
import { HiOutlineArrowLeft, HiPlus } from "react-icons/hi";

import InputText from "../../components/Form2/InputText";

import useSWRMutation from "swr/mutation";
import { sendRequestPostWithToken } from "../../swr";
import Loading from "../Loading";

const FormAdd = () => {
  const [kodeModelKendaraan, setKodeModelKendaraan] = useState("");
  const [namaModelKendaraan, setNamaModelKendaraan] = useState("");

  const navigate = useNavigate();

  const { trigger, isMutating } = useSWRMutation(
    `${API_URL_MASTER}/modelk`,
    sendRequestPostWithToken
  );

  useEffect(() => {
    // get refresh token
    checkToken();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const checkToken = () => {
    const token = localStorage.getItem("token");
    if (token) {
      const decoded = jwtDecode(token);

      const currentDate = new Date();
      if (decoded.exp * 1000 < currentDate.getTime()) {
        navigate("/login");
      }
    } else {
      navigate("/login");
    }
  };

  const simpan = async (e) => {
    e.preventDefault();

    try {
      const result = await trigger({
        kodeModelKendaraan: kodeModelKendaraan,
        namaModelKendaraan: namaModelKendaraan,
      });

      Swal.fire({
        icon: "success",
        title: "Sukses!",
        text: result.message,
        confirmButtonText: "Oke",
        willClose: () => {
          navigate(-1);
        },
      });
    } catch (e) {
      if (e.status > 400) {
        Swal.fire("Error!", e.info.message, "error");
      } else {
        Swal.fire("Error!", "Error, Silahkan ulangi kembali!", "error");
      }
    }
  };

  if (isMutating) return <Loading />;

  return (
    <div className="mx-3 px-4 py-4 rounded-lg drop-shadow-lg bg-white">
      <div className="border-b">
        <h2 className="font-sans font-bold text-lg text-gray-800">
          TAMBAH MODEL KENDARAAN
        </h2>
        <p className="text-sm font-sans text-gray-500">
          Penambahan Model Kendaraan.
        </p>
      </div>
      <form onSubmit={simpan}>
        <div className="mt-5 grid grid-cols-1 gap-x-4 gap-y-3 sm:grid-cols-6">
          <div className="sm:col-start-1 sm:col-span-2">
            <InputText
              label="Kode Model Kendaraan"
              name="kodeModelKendaraan"
              val={kodeModelKendaraan}
              set={setKodeModelKendaraan}
              placeholder="Masukkan Kode Model Kendaraan"
            />
          </div>
          <div className="sm:col-start-1 sm:col-span-4">
            <InputText
              label="Nama Model Kendaraan"
              name="namaModelKendaraan"
              val={namaModelKendaraan}
              set={setNamaModelKendaraan}
              placeholder="Masukkan Nama Model Kendaraan"
            />
          </div>
        </div>
        <div className="mt-10 flex items-center gap-x-2">
          <button
            type="button"
            className="text-gray-700 bg-[#fbbf24] hover:bg-[#fbbf24]/90 active::bg-[#fbbf24]/50  focus:outline-none focus:ring-[#b91c1c]/50 font-sans font-bold rounded-lg text-sm px-6 py-2 text-center inline-flex items-center shadow-md"
            onClick={() => navigate(-1)}
          >
            <span className="mr-2">
              <HiOutlineArrowLeft size={18} />
            </span>
            <span>Cancel</span>
          </button>
          <button
            type="submit"
            disabled={isMutating}
            className="text-white bg-[#1d4ed8] hover:bg-[#1d4ed8]/90 active::bg-[#1d4ed8]/50  focus:outline-none focus:ring-[#1d4ed8]/50 font-sans font-bold rounded-lg text-sm px-10 py-2 text-center inline-flex items-center shadow-md"
          >
            <span className="mr-2">
              <HiPlus size={18} />
            </span>
            <span>Simpan Model Kendaraan</span>
          </button>
        </div>
      </form>
    </div>
  );
};

export default FormAdd;
