import React, { useState, useEffect } from "react";
import jwtDecode from "jwt-decode";
import { Link, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

import { API_URL_MASTER } from "../../config/index";
import {
  HiOutlineRefresh,
  HiOutlinePlusSm,
  HiOutlineSearch,
  HiPencilAlt,
  HiTrash,
} from "react-icons/hi";

import EmptyData from "../ErrorPage/EmptyData";

import useSWR from "swr";
import useSWRMutation from "swr/mutation";
import { sendRequestDelete, sendRequestGet } from "../../swr";
import Loading from "../Loading";
import Paging from "../../components/Paging";
import ButtonDropdown from "../../components/ButtonDropdown";

const Table = () => {
  const navigate = useNavigate();

  const [page, setPage] = useState(0);
  const [itemPage, setItemPage] = useState(0);
  /* eslint-disable-next-line */
  const [limit, setLimit] = useState(10);
  const [search, setSearch] = useState("");
  const [searchQuery, setSearchQuery] = useState("");

  const { trigger, isMutating } = useSWRMutation(
    `${API_URL_MASTER}/modelk`,
    sendRequestDelete
  );

  const { data, error, isLoading, mutate } = useSWR(
    `${API_URL_MASTER}/modelk?search=${searchQuery}&page=${page}&limit=${limit}`,
    sendRequestGet
  );

  // console.log(data);

  useEffect(() => {
    // get refresh token
    checkToken();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const checkToken = () => {
    const token = localStorage.getItem("token");
    if (token) {
      const decoded = jwtDecode(token);

      const currentDate = new Date();
      if (decoded.exp * 1000 < currentDate.getTime()) {
        navigate("/login");
      }
    } else {
      navigate("/login");
    }
  };

  const deleteData = async (id) => {
    Swal.fire({
      title: "Konfirmasi Hapus?",
      text: "Yakin ingin menghapus data!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, hapus data!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          const result = await trigger({ id: id });

          mutate();

          Swal.fire("Deleted!", result.message, "success");
        } catch (e) {
          Swal.fire("Deleted!", e.info.message, "error");
        }
      }
    });
  };

  const changePage = ({ selected }) => {
    setItemPage(selected);
    setPage(selected + 1);
  };

  const searchData = (e) => {
    e.preventDefault();
    mutate();
    setPage(0);
    setItemPage(0);
    setSearchQuery(search);
  };

  const resetPage = (e) => {
    e.preventDefault();
    mutate();
    setPage(0);
    setItemPage(0);
    setSearch("");
    setSearchQuery("");
    setLimit(10);
  };

  if (isLoading || isMutating) return <Loading />;

  if (error)
    return (
      <EmptyData
        msgTitle="Data tidak ditemukan!"
        msgDesc="Silahkan klik tombol dibawah ini untuk menambahakan data"
        button={
          <Link
            to="add"
            className="text-white bg-[#2563eb] hover:bg-[#2563eb]/90 active::bg-[#2563eb]/50  focus:outline-none focus:ring-[#2563eb]/50 rounded-lg font-sans font-bold text-sm px-5 py-2 text-center inline-flex items-center shadow-md"
          >
            <span className="mr-2">
              <HiOutlinePlusSm size={18} />
            </span>
            <span>Tambah Data Baru</span>
          </Link>
        }
      />
    );

  return (
    <>
      <div className="px-3 mb-3">
        <div className="flex flex-row items-center">
          <div className="flex-1 mr-3 font-sans font-bold text-lg">
            Daftar Model Kendaraan
          </div>
          <div className="flex-1 text-end">
            <button
              type="submit"
              className="text-gray-500 bg-white hover:bg-gray-300 active:bg-gray-100 active:text-gray-300 focus:ring-4 focus:outline-none focus:ring-[#F9FAFB]/50 rounded-lg text-sm px-5 py-2 text-center font-sans font-bold inline-flex items-center mr-2 shadow-md"
              onClick={resetPage}
            >
              <span className="mr-2">
                <HiOutlineRefresh size={18} />
              </span>
              <span>Reset</span>
            </button>
            <Link
              to="add"
              className="text-white bg-[#2563eb] hover:bg-[#2563eb]/90 active::bg-[#2563eb]/50  focus:outline-none focus:ring-[#2563eb]/50 rounded-lg font-sans font-bold text-sm px-5 py-2 text-center inline-flex items-center shadow-md"
            >
              <span className="mr-2">
                <HiOutlinePlusSm size={18} />
              </span>
              <span>Tambah</span>
            </Link>
          </div>
        </div>
      </div>
      <div className="drop-shadow-lg bg-white mx-3 rounded-lg min-h-96">
        <div className="relative">
          <div className="px-4 py-2 border-b">
            <h2 className="font-sans font-bold text-lg text-blue-600">
              Semua Daftar Model Kendaraan
            </h2>
          </div>
          <div className="flex flex-row px-4 py-2">
            <form onSubmit={searchData}>
              <div className="flex">
                <div className="relative w-full">
                  <input
                    type="search"
                    id="search-dropdown"
                    className="block px-4 py-2 ps-8 w-full z-20 font-sans text-sm text-gray-900 bg-white rounded-lg border border-gray-300 focus:ring-gray-300 focus:bg-gray-50 focus:border-blue-500"
                    placeholder="Search..."
                    value={search}
                    onChange={(e) => setSearch(e.target.value)}
                  />
                  <div className="absolute inset-y-0 start-2 flex items-center">
                    <HiOutlineSearch size={24} color="#9ca3af" />
                  </div>
                </div>
              </div>
            </form>
          </div>
          <table className="w-full font-sans text-xs text-left text-gray-500 border-b">
            <thead className="border-b border-t text-gray-600 bg-white">
              <tr>
                <th scope="col" className="p-2">
                  #
                </th>
                <th scope="col" className="px-2 py-3">
                  KODE MODEL KENDARAAN
                </th>
                <th scope="col" className="px-2 py-3">
                  NAMA MODEL KENDARAAN
                </th>
                <th scope="col" className="px-2 py-3">
                  CREATED AT
                </th>
                <th scope="col" className="px-2 py-3">
                  UPDATED AT
                </th>
                <th scope="col" className="px-2 py-3">
                  <span>AKSI</span>
                </th>
              </tr>
            </thead>
            <tbody>
              {data ? (
                <>
                  {data.data.map((val, index) => {
                    return (
                      <tr
                        key={index}
                        className="bg-white border-b hover:bg-gray-50"
                      >
                        <td className="p-2">
                          <div className="flex items-center">
                            <input
                              id="checkbox-all-search"
                              type="checkbox"
                              className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"
                            />
                            <label
                              htmlFor="checkbox-all-search"
                              className="sr-only"
                            >
                              checkbox
                            </label>
                          </div>
                        </td>
                        <td className="px-2 py-2 text-gray-900">
                          <strong>{val.kodeModelKendaraan}</strong>
                        </td>
                        <td className="px-2 py-2">{val.namaModelKendaraan}</td>
                        <td className="px-2 py-2">{val.createdAt}</td>
                        <td className="px-2 py-2">{val.updatedAt}</td>
                        <td className="px-2 py-2 text-center">
                          <ButtonDropdown
                            index={index}
                            listMenu={[
                              {
                                id: val.kodeModelKendaraan,
                                namaMenu: "Edit",
                                icon: <HiPencilAlt size={14} color="#9ca3af" />,
                                url: `${val.kodeModelKendaraan}`,
                                type: "link",
                              },
                              // {
                              //   id: val.kodeModelKendaraan,
                              //   namaMenu: "Hapus",
                              //   icon: <HiTrash size={14} color="#9ca3af" />,
                              //   onClick: () => deleteData(val.kodeModelKendaraan),
                              //   type: "button",
                              // },
                            ]}
                          />
                        </td>
                      </tr>
                    );
                  })}
                </>
              ) : (
                <tr className="bg-white border-b hover:bg-gray-50">
                  <td colSpan="6" className="px-2 py-2 text-center">
                    Data tidak ditemukan
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        {!isLoading && (
          <Paging
            totalRows={data.totalRows}
            totalPages={data.totalPage}
            page={itemPage}
            limit={limit}
            setLimit={setLimit}
            changePage={changePage}
          />
        )}
      </div>
    </>
  );
};

export default Table;
